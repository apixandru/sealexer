#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "lexer.h"
#include "files.h"


FILE *readFileFromArg(int argc, char **argv) {
    if (argc != 2) {
        printf("Needs exactly one argument, the file name.\n");
        exit(1);
    }
    return openFile(*++argv);
}

int main(int argc, char **argv) {
    FILE *fp = readFileFromArg(argc, argv);
    InputSource source = toInputSource(fp);
    while (true) {
        Token *token = nextToken(&source);
        if (token == NULL) {
            printf("%s\n", source.buffer);
            break;
        }
        printToken(token);
        if (token->type == T_EOF) {
            break;
        }
    }
    return 0;
}
