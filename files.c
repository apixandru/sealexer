#include <stdlib.h>
#include "files.h"

FILE *openFile(char *fileName) {
    FILE *fp = fopen(fileName, "r");
    if (fp == NULL) {
        printf("Cannot open %s", fileName);
        exit(1);
    }
    return fp;
}
