#include <stdbool.h>
#include <memory.h>
#include <stdlib.h>
#include "lexer.h"
#include "files.h"

static char *buffer;
static size_t charsRead;

static void trim(char *buffer) {
    size_t chars = strlen(buffer);
    for (size_t i = chars - 1; i > 0; i--) {
        if (isWhiteSpace(buffer[i])) {
            buffer[i] = '\0';
        } else {
            return;
        }
    }
}

static void readline(FILE *fpt) {
    getline(&buffer, &charsRead, fpt);
    trim(buffer);
}

static bool test(char *testName) {
    strcpy(buffer, testName);
    strcat(buffer, ".sea");
    FILE *fps = openFile(buffer);
    strcpy(buffer, testName);
    strcat(buffer, ".tokens");
    FILE *fpt = openFile(buffer);

    InputSource source = toInputSource(fps);
    int line = 0;
    while (true) {
        line++;
        Token *token = nextToken(&source);
        if (token == NULL) {
            printf("%s", source.buffer);
            return false;
        }
        readline(fpt);

        char *string = tokenToString(token);
        trim(string);
//        printf("%s\n", string);
        if (strcmp(buffer, string) != 0) {
            printf("Line %d of %s differs\n", line, testName);
            printf("Expected: %s\n", buffer);
            printf("Actual:   %s\n", string);
            exit(1);
        }
        free(string);
        if (token->type == T_EOF) {
            break;
        }
    }
    return true;
}

int main(int argc, char **argv) {
    buffer = (char *) malloc(4096);
    if (buffer == NULL) {
        printf("Can't allocate enough for the buffer!\n");
        exit(1);
    }
     bool result = test("../test-files/test-001");

    if (result) {
        printf("All tests passed!\n");
    } else {
        printf("Encountered test failures. \n");
    }
    return 0;
}
