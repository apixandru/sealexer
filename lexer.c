#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <ctype.h>
#include "lexer.h"

static const char *tokenTypeNames[] = {
        "T_EOF",
        "T_COMMA",
        "T_KEYWORD",
        "T_IDENTIFIER",
        "T_NUMBER",
        "T_OPERATOR",
        "T_TYPE",
        "T_BEGINBLOCK",
        "T_ENDBLOCK",
        "T_LBRACK",
        "T_RBRACK",
        "T_STRING"
};

static const char *typeName[] = {"int", "string", "void"};

static const char *keywordName[] = {
        "if", "else", "loop", "for", "while", "repeat",
        "until", "do", "break", "run", "return"
};

bool isWhiteSpace(char c) {
    return (c == ' ' || c == '\t' || c == '\n' || c == '\r');
}

static bool isKeyword(char *name) {
    int to = sizeof(keywordName) / sizeof(keywordName[0]);
    for (int i = 0; i < to; i++) {
        if (strcmp(name, keywordName[i]) == 0) {
            return true;
        }
    }
    return false;
}

static bool isType(char *name) {
    int to = sizeof(typeName) / sizeof(typeName[0]);
    for (int i = 0; i < to; i++) {
        if (strcmp(name, typeName[i]) == 0) {
            return true;
        }
    }
    return false;
}

char consume(InputSource *source) {
    char temp = source->currentChar;
    if (source->currentChar == '\n') {
        source->line++;
        source->character = 0;
    }
    source->currentChar = (char) fgetc(source->fp);
    if (source->currentChar == '\r') {
        // this is here because just printing \r causes weird console
        // behavior, at least in clion it just kills the output
        source->currentChar = ' ';
    }

    source->character++;
    return temp;
}

bool match(InputSource *input, char c) {
    if (c == input->currentChar) {
        consume(input);
        return true;
    } else {
        sprintf(input->buffer, "ERR: Expecting '%c' but got '%c' at (%2d,%2d)\n",
                c, input->currentChar, input->line, input->character);
        return false;
    }
}

bool consumeAndMatch(InputSource *input, char c) {
    consume(input);
    return match(input, c);
}

void consumeComments(InputSource *input) {
    while (input->currentChar != '\n' && input->currentChar != EOF) {
        consume(input);
    }
}

void consumeBlockComments(InputSource *input) {
    char previousCharacter = ' ';
    while (input->currentChar != EOF) {
        if (input->currentChar == '/' && previousCharacter == '*') {
            consume(input);
            return;
        }
        previousCharacter = input->currentChar;
        consume(input);
    }
}

InputSource toInputSource(FILE *fp) {
    InputSource source;
    source.fp = fp;
    source.line = 1;
    source.character = 0;
    consume(&source);
    return source;
}

void consumeWhiteSpace(InputSource *input) {
    while (isWhiteSpace(input->currentChar)) {
        consume(input);
    }
}

Token *newToken(InputSource *input, char *c, TokenType type) {
    Token *token = malloc(sizeof(Token));
    if (token == NULL) {
        printf("Can't allocate enough memory for token\n");
        exit(1);
    }
    token->line = input->tokenLine;
    token->character = input->tokenCharacter;
    token->type = type;
    token->value = c;
    return token;
}

Token *newCharToken(InputSource *input, char c, TokenType type) {
    char string[] = {c, '\0'};
    return newToken(input, strdup(string), type);
}

void printToken(Token *token) {
    const char *tokenTypeName = tokenTypeNames[token->type];
    printf("(%4d,%4d) %10s: %s\n",
           token->line, token->character, tokenTypeName, token->value);
}

char *tokenToString(Token *token) {
    static char buffer[4096];
    const char *tokenTypeName = tokenTypeNames[token->type];
    sprintf(buffer, "(%2d,%2d) %12s: %s\n",
            token->line, token->character, tokenTypeName, token->value);
    return strdup(buffer);
}

char *parseString(InputSource *inputSource) {
    int pos = 0;
    while (inputSource->currentChar != EOF && inputSource->currentChar != '\n') {
        inputSource->buffer[pos++] = inputSource->currentChar;
        consume(inputSource);
        if (inputSource->currentChar == '"' && inputSource->buffer[pos - 1] != '\\') {
            inputSource->buffer[pos++] = inputSource->currentChar;
            consume(inputSource);
            inputSource->buffer[pos] = '\0';
            return strdup(inputSource->buffer);
        }
    }
    printf("Reached EOF and still no ending string character.\n");
    exit(1);
}

static char *parseConditionally(InputSource *inputSource, int (*test)(int)) {
    int pos = 0;
    while (inputSource->currentChar != EOF && test(inputSource->currentChar)) {
        inputSource->buffer[pos++] = inputSource->currentChar;
        consume(inputSource);
    }
    inputSource->buffer[pos] = '\0';
    return strdup(inputSource->buffer);
}

char *parseNumber(InputSource *inputSource) {
    return parseConditionally(inputSource, &isdigit);
}

char *parseAlphanumeric(InputSource *inputSource) {
    return parseConditionally(inputSource, &isalnum);
}

Token *newAlphaNumericToken(InputSource *input) {
    char *name = parseAlphanumeric(input);
    TokenType type = isKeyword(name) ? T_KEYWORD : isType(name) ? T_TYPE : T_IDENTIFIER;
    return newToken(input, name, type);
}

Token *newOperatorToken(InputSource *input) {
    input->buffer[0] = input->currentChar;
    int pos = 1;
    consume(input);
    if (input->currentChar == '=') {
        consume(input);
        input->buffer[pos++] = '=';
    }
    input->buffer[pos] = '\0';
    char *string = strdup(input->buffer);
    return newToken(input, string, T_OPERATOR);
}

Token *newOperatorOrComment(InputSource *input) {
    input->buffer[0] = input->currentChar;
    consume(input);
    if (input->currentChar == '/') {
        consumeComments(input);
        return NULL;
    } else if (input->currentChar == '*') {
        consumeBlockComments(input);
        return NULL;
    }
    int pos = 1;
    if (input->currentChar == '=') {
        consume(input);
        input->buffer[pos++] = '=';
    }
    input->buffer[pos] = '\0';
    char *string = strdup(input->buffer);
    return newToken(input, string, T_OPERATOR);
}

Token *nextToken(InputSource *input) {
    bool keepGoing = true;
    while (keepGoing) {
        consumeWhiteSpace(input);
        input->tokenLine = input->line;
        input->tokenCharacter = input->character;
        switch (input->currentChar) {
            case '{':
                return newCharToken(input, consume(input), T_BEGINBLOCK);
            case '}':
                return newCharToken(input, consume(input), T_ENDBLOCK);
            case '(':
                return newCharToken(input, consume(input), T_LBRACK);
            case ')':
                return newCharToken(input, consume(input), T_RBRACK);
            case ',':
                return newCharToken(input, consume(input), T_COMMA);
            case '"':;
                return newToken(input, parseString(input), T_STRING);
            case EOF:
                return newToken(input, "EOF", T_EOF);
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                return newToken(input, parseNumber(input), T_NUMBER);
            case '|':
                if (consumeAndMatch(input, '|')) {
                    return newToken(input, "||", T_OPERATOR);
                }
                return NULL;
            case '&':
                if (consumeAndMatch(input, '&')) {
                    return newToken(input, "&&", T_OPERATOR);
                }
                return NULL;
            case '/': {
                Token *token = newOperatorOrComment(input);
                if (token == NULL) {
                    continue;
                }
                return token;
            }
            case '%':
                return newCharToken(input, '%', T_OPERATOR);
            case '+':
            case '!':
            case '-':
            case '=':
            case '*':
            case '<':
            case '>':
                return newOperatorToken(input);
            case '#':
                consumeComments(input);
            case ';':
                continue;
            default:
                if (isalpha(input->currentChar)) {
                    return newAlphaNumericToken(input);
                }
                keepGoing = false;
                break;
        }
    }
    sprintf(input->buffer, "ERR: Unexpected character: %c (%d) at %d,%d\n",
            input->currentChar,
            input->currentChar,
            input->line,
            input->character);
    return NULL;
}
