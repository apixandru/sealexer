#ifndef SEALEXER_LEXER_H
#define SEALEXER_LEXER_H

#include <stdio.h>
#include <stdbool.h>

typedef enum {
    T_EOF,
    T_COMMA,
    T_KEYWORD,
    T_IDENTIFIER,
    T_NUMBER,
    T_OPERATOR,
    T_TYPE,
    T_BEGINBLOCK,
    T_ENDBLOCK,
    T_LBRACK,
    T_RBRACK,
    T_STRING
} TokenType;

typedef struct {
    TokenType type;
    int line;
    int character;
    char *value;
} Token;

typedef struct {
    char buffer[4096];
    FILE *fp;
    int line;
    int character;
    int tokenLine;
    int tokenCharacter;
    char currentChar;
} InputSource;

InputSource toInputSource(FILE *fp);

void printToken(Token *token);

char *tokenToString(Token *token);

Token *nextToken(InputSource *input);

bool isWhiteSpace(char c);

#endif //SEALEXER_LEXER_H
